# Description

**`gnome-codesearch`** is a userscript that enhances the [GNOME online C API](https://developer.gnome.org/references) by providing useful links to corresponding source code in the [codesearch.debian.net](https://codesearch.debian.net/about) service. You will find those links alongside any `function`, `macro`, `signal`, `property` or `enum` symbol in the aforementioned [GNOME online C API](https://developer.gnome.org/references).

What's more, once you are browsing the corresponding source file, you'll notice that every `function` or `macro` call that you mouse-over will get auto-linked so you can quickly navigate to its source code too.

# Install
1. Install one of *Greasemonkey* or *Tampermonkey* extension in your browser. They are well known extensions that you can easily install from inside your browser's extension/addon menu.
2. Click the following link to the raw **gnome-codesearch** userscript in this repo: [**gnome-codesearch.user.js**](https://gitlab.gnome.org/nbenitez/gnome-codesearch/-/raw/master/gnome-codesearch.user.js)
3. Navigate to the [GNOME online C API](https://developer.gnome.org/references) and start browsing your now enhanced API documentation.
4. Profit!! :slight_smile:

# Short Demo Video
This short video shows how **gnome-codesearch** allows you to navigate through GNOME source code.
![gnome-codesearch-demo](/uploads/7a04539e0d10c43f31c3f6e1e1565f15/gnome-codesearch-demo.webm)

# Author
Nelson Benítez León

# Thanks
To [Michael Stapelberg](https://michael.stapelberg.ch/) for providing the foss community with a much needed and useful [code search service](https://codesearch.debian.net/).
