// ==UserScript==
// @name        gnome-codesearch
// @description Cross-reference code browsing from GNOME api docs to codesearch.debian.net
// @icon        http://icons.iconarchive.com/icons/papirus-team/papirus-apps/32/desktop-environment-gnome-icon.png
// @namespace   org.gnome.nbenitez
// @require     https://dcs.zekjur.net/jquery.min.js
// @include     /^https://developer.gnome.org/.+/.+/.+\.html/
// @include     /^https://(codesearch\.debian|dcs\.zekjur).net/search.+/
// @include     /^https://sources\.debian\.org/src/.+/
// @grant       none
// @version     1.0
// ==/UserScript==

//Another option is to embed jQuery 1.8.2 taken from http://userscripts.org/scripts/show/149429

$.FUNCS = {}; //hash table for func and macro names

if (typeof GM_addStyle == 'undefined') {
  this.GM_addStyle = (aCss) => {
    'use strict';
    let head = document.getElementsByTagName('head')[0];
    if (head) {
      let style = document.createElement('style');
      style.setAttribute('type', 'text/css');
      style.textContent = aCss;
      head.appendChild(style);
      return style;
    }
    return null;
  }
}
  
GM_addStyle ( `
  a.gcsa {
    border-bottom: none !important ;
    text-decoration: none !important ;
  }

  a.gcsa:hover {
    text-decoration: underline !important ;
  }
` );
    
String.prototype.replaceAll = function(f,r){return this.split(f).join(r);}

function get_link(type, symbol, link_type) {
  
  var SERVERS = ['https://codesearch.debian.net/','https://dcs.zekjur.net/'];
  var REGEX = {};
  var path_c = "%20path%3A%5C.c%24";//search only in .c files
  var path_ui = "%20path%3A%5C.ui%24";//search only in .ui files
  var path_c_cpp_cxx = "%20-path%3A%5C.h%24%20filetype%3Ac%20filetype%3Ac%2B%2B";//searches in .c .cpp .cxx files (.h excluded)
  var path_c_h = "%20filetype%3Ac";//search only in .h and .c files
  var path_c_cpp_cxx_h = "%20filetype%3Ac%20filetype%3Ac%2B%2B";//these include searches in .h .c .cpp .cxx files
  
  if (type == 'function') {
    
    REGEX.function_source = "@SERVER@search?q=%28%3Fm%3A%5E@TEXT@%29%5Cs%2A%5C%28%28%3Fm%3A%28%5B%5E%3B%5D%2B%29%24%29" + path_c;
    REGEX.function_uses = "@SERVER@search?q=%28%3Fm%3A%5E%28%3F%3A%5Ct%7C%20%29%2A%29%5B%5E%5C%2A%5Ct%2F%20%5D%7B1%7D.%2A@TEXT@%20%2A%5C%28" + path_c_cpp_cxx;

  } else if (type == 'macro') {
    
    REGEX.macro_source = "@SERVER@search?q=%28%3Fi%3A%23define%20@TEXT@%29%28%3F%3A%20%7C%5C%28%29%7B1%7D" + path_c_h;//(?i:#define @TEXT@)(?: |\(){1}
    // The following (macro_uses) regex avoids matches that are inside comments(cool) (but with the downside of including
    // the macro definition match too (but it's good for cases when macro is used in the definition of another macro))
    REGEX.macro_uses = "@SERVER@search?q=%28%3Fm%3A%5E%28%3F%3A%5Ct%7C%20%29%2A%29%5B%5E%5C%2A%5Ct%2F%20%5D%7B1%7D.%2A@TEXT@%5CW%7B1%7D" + path_c_cpp_cxx_h;

  } else if (type == 'signal') {
    
    var r = symbol.split('-').map(function(el){ return '(?i:'+el+')' }).join('(?:-|_)'); //signal-name => (?i:signal)(?:-|_)(?i:name)
    //REGEX.signal_uses1 = "@SERVER@search?q=g_signal_c.%2B%28%3F%3A%5C%22%7C%5C%5B%29%7B1%7D"+encodeURIComponent(r)+
    REGEX.signal_uses = "@SERVER@search?q=%28%3F%3Ag_signal_c.%2B%7C%5B%5Ct%20%5D%2B%29%28%3F%3A%5C%22%7C%5C%5B%29%7B1%7D"+encodeURIComponent(r)+
                        "%28%3F%3A%5C%22%7C%5C%5D%29%20%3F%2C" + path_c; // Connect uses
    //REGEX.signal_uses2 = "@SERVER@search?q=%28%3Fm%3A%5E%29%28%3F%3A%20%7C%5Ct%29%2B%28%3F%3A%22%7C%5C%5B%29%7B1%7D"+
    //                      encodeURIComponent(r)+"%28%3F%3A%5C%22%7C%5C%5D%29%20%3F%2C"+path; // (More)
    REGEX.signal_uses1 = "@SERVER@search?q=%3Csignal%20.%2Aname%3D.%3F"+encodeURIComponent(r)+
                         ".%3F%28%3F%3A%20%7C%2F%7C%3E%29%7B1%7D" + path_ui;// Connect in .ui files
    REGEX.signal_uses3 = "@SERVER@search?q=g_signal_%5B%5Ec%5D%7B1%7D.%2B%28%3F%3A%5C%22%7C%5C%5B%29%7B1%7D"+encodeURIComponent(r)+
                         "%28%3F%3A%5C%22%7C%5C%5D%29%20%3F%2C" + path_c;// Other uses
    REGEX.signal_source ="@SERVER@search?q=g_signal_new.%2B%28%3F%3A%5C%22%7C%5C%5B%29%7B1%7D"+encodeURIComponent(r)+
                         "%28%3F%3A%5C%22%7C%5C%5D%29%20%3F%28%3F%3A%2C%7C%5C%29%29" + path_c; // Definition

  } else if (type == 'struct') {

    REGEX.struct_source = "@SERVER@search?q=%28%3Fm%29%28%3F%3A%5Ct%7C%20%29%2A%28%3F%3Astruct%20%7C%5C%7D%29%7B1%7D%20%2A_%3F@TEXT@%28%3Fi%3APrivate%29%3F%3B%3F%20%2A%7B%3F%20%2A%24";
    REGEX.struct_source += path_c_h;
    REGEX.struct_uses = "@SERVER@search?q=_%3F@TEXT@%28%3Fi%3APrivate%29%3F%20%5Ba-zA-Z0-9_%5D%2B%28%3F%3A%2C%5Ba-zA-Z0-9_%5D%2B%3B%7C%3B%29%7B1%7D" + path_c_h;

  } else if (type == 'enum') {

    REGEX.enum_source = "@SERVER@search?q=%28%3F%3A%20%7C%7D%29%7B1%7D@TEXT@%3B" + path_c_h;
    REGEX.enum_uses   = "@SERVER@search?q=_%3F@TEXT@%28%3Fi%3APrivate%29%3F%20%5Ba-zA-Z0-9_%5D%2B%28%3F%3A%2C%5Ba-zA-Z0-9_%5D%2B%3B%7C%3B%29%7B1%7D" + path_c_h;
    REGEX.enum_uses1  = "@SERVER@search?q=@TEXT@" + path_c_cpp_cxx;

  } else if (type == 'property') {

    var r = symbol.split('-').map(function(el){ return '(?i:'+el+')' }).join('(?:-|_)');
    REGEX.property_source = "@SERVER@search?q=g_param_spec.%2A%28%3F%3A%22%7C%5C%5B%29"+encodeURIComponent(r)+"%28%3F%3A%22%7C%5C%5D%29"+path_c_cpp_cxx;
    REGEX.property_uses   = "@SERVER@search?q=%28%3F%3A%22%7C%5C%5B%29"+encodeURIComponent(r) + "%28%3F%3A%22%7C%5C%5D%29" + path_c_cpp_cxx;
    REGEX.property_uses1  = "@SERVER@search?q=%3D%20%2A%5CW%7B1%7D"+encodeURIComponent(r) + "%5CW%7B1%7D" + path_ui;
  }
  
  var url_parameters="&literal=0";
  var n = 1; //defaulting to the Rackspace one, but eventually we can make it round robin.
  var reg = type + '_' + link_type;
  
  if (!REGEX.hasOwnProperty(reg))
    return '';
  
  //remove first character (needed by '(macro|function)_uses' regexes) to avoid having results coming from inside of comments)
  if (reg == 'macro_uses' || reg == 'function_uses')
    symbol = symbol.substr(1);
                  
  return REGEX[reg].replace('@TEXT@', symbol).replace('@SERVER@', SERVERS[n])+url_parameters;
}

function element_info(el, h3) {
  var empty = {type:'', symbol:''};
  //var h3 = $('h3:first', el);
  if (!h3.length) return empty;
  
  var text = h3.text();
  
  if (text.indexOf('struct ') == 0 || h3.prev('a').attr('name').indexOf('-struct') > 0 ||
      h3.next('pre.programlisting').text().indexOf('typedef') === 0)
    return {type:'struct', symbol:text.replace('struct ','') };
  
  if (text.indexOf('enum ') == 0)
    return {type:'enum',   symbol:text.substr(5) };
  
  if (text.indexOf('” property') > 0)
    return {type:'property', symbol:text.substring(text.indexOf('“')+1, text.indexOf('”')) };
  
  if (text.indexOf('” signal') > 0)
    return {type:'signal', symbol:text.substring(text.indexOf('“')+1, text.indexOf('”')) };
  
  var pl = $('.programlisting:first', el);
  
  if (pl.length && !pl.parents('.listing_code').length) {
    var pl_text = pl.text().toLowerCase();
    var is_macro = pl_text.indexOf('#define') === 0 ? true : false;
    var is_function = !is_macro;
    //if (is_function && pl_text.indexOf('(void)') > 0) parameterless = true;
    var symbol = h3.prev('a').attr('name').split(':').shift().replaceAll('-','_');
    //if (is_macro && pl_text.indexOf(symbol.toLowerCase()+'(') < 0) parameterless = true;

    return {type:(is_macro?'macro':'function'), symbol: symbol};
  }
  
  return empty;
}

//
// SCRIPT STARTS HERE
//
if (location.hostname == 'developer.gnome.org') {
  
  $('#container').on('mouseover','.refsect2', function() {
    var extlink = $('.extlink:first', this);
    if (extlink.length) {
      extlink.show();
      return;
    }
    var h3 = $('h3:first', this);
    const { type: type, symbol: symbol} = element_info(this, h3);
    if (type == '')
      return;
    //console.log('type:'+type+' symbol:'+symbol);
    var href_source = get_link(type, symbol, 'source');
    var href_uses = get_link(type, symbol, 'uses');
    
    if (type == 'signal') {
      var href_uses1 = get_link(type, symbol, 'uses1');
      //var href_uses2 = get_link(type, symbol, 'uses2');
      var href_uses3 = get_link(type, symbol, 'uses3');
      //<a style="color:#0769ad; vertical-align:super;font-size:smaller" target="_blank" href="'+href_uses2+'">(more)</a>&nbsp;&nbsp;\
      h3.append('<span style="padding-left:25px;font-size:70%; color:#204a87" class="extlink">\
                 code search: &nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_uses+'"><i>Connect uses</i></a>&nbsp;&nbsp;\
                 |&nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_uses1+'"><i>In .ui files</i></a>&nbsp;&nbsp;\
                 |&nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_uses3+'"><i>Other uses</i></a>&nbsp;&nbsp;\
                 |&nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_source+'"><i>Definition</i></a></span>');
    } else {
      var st = type == 'struct' ? ' <span style="color:#0769ad; vertical-align:super;font-size:smaller">(including any private wrapper)</span>' : '';
      var pui = type == 'property' ? '&nbsp;&nbsp;|&nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+
                                      get_link(type,symbol,'uses1')+'"><i>In .ui files</i></a>' : '';
      h3.append('<span style="padding-left:25px;font-size:70%; color:#204a87" class="extlink">\
                 code search: &nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_source+'"><i>Definition</i></a>'+st+'&nbsp;&nbsp;\
                 |&nbsp;&nbsp;<a style="color:#0769ad" target="_blank" href="'+href_uses+'"><i>Uses</i></a>'+pui+'</span>');
    }

  }).on('mouseout','.refsect2', function() {

    $('.extlink:first', this).hide();
    
  }).on('mouseover','.enum_member_name', function() {

    var extlink = $('.extlink:first', this);
    if (extlink.length) {
      extlink.show();
      return;
    }
    var href_uses1 = get_link('enum', $(this).text().trim(), 'uses1');
    $('p:first', this).append('&nbsp;&nbsp;<span class="extlink"><a style="color:#0769ad" target="_blank" href="'+ href_uses1 +'"><i>Uses</i></a></span>');

  }).on('mouseout','.enum_member_name', function() {

    $('.extlink:first', this).hide();

  });

} else if (location.pathname == '/search') { //codesearch.debian.net or mirrors
  
  $('#content').on('click','.showhint', function(ev){
    
    var el = $(this);
    var span = el.prev('span');
    var value = span.css('white-space') == 'normal' ? 'nowrap': 'normal';
    span.css('white-space',value);
    
  }).on('mouseover','pre:not(.replaced)', function(ev){
    
    var replaced = $(this).addClass('replaced').html().replace(/(G_CALLBACK ?\()([^)]+)(\))/g, function (s, p1, p2, p3) {
      return p1 + '<a target="_blank" class="gcsa" href="'+ get_link('function', p2, 'source') + '">'+p2+'</a>' + p3;
    });
    $(this).html(replaced);

  });
  
} else if (location.hostname == 'sources.debian.org') {

  $('.codeline').each(function() {
    var text = $(this).text();
    if (text.substr(0,8) == '#define ') {
      var ind = text.indexOf('(');
      if (ind > 0) {
         var macro = text.substring(8, ind).trim();
         $.FUNCS[macro] = $(this).attr('id');
      }
    } else {
      var match = text.match(/^([^\t ]{1}[^ ]+) ?\(.*(?:,|\)) *(?:\n|\r\n|\r)$/);
      if (match) {
         $.FUNCS[match[1]] = $(this).attr('id');
      }
    }
  });
  
  $('#content').on('mouseover','.codeline:not(.replaced)',function() {

    var replaced = $(this).addClass('replaced').html().replace(/([a-zA-Z0-9_]+)( ?\()/g, function (s, p1, p2) {
      if ($.FUNCS[p1]) {
        return '<a class="gcsa" href="#'+ $.FUNCS[p1].replace('line','L') + '">'+p1+'</a>' + p2;
      } else {
        var type = p1.toUpperCase() == p1 ? 'macro' : 'function';
        return '<a target="_blank" class="gcsa" href="'+ get_link(type, p1, 'source') + '">'+p1+'</a>' + p2;
      }
    });
    $(this).html(replaced);

  });
}
